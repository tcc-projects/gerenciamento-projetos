## Descrição:
Este software prove uma interface REST para criação e recuperação de projetos da prefeitura afim de trazer transparencia do planejamento da prefeitura para os munícipes 

## Tecnologias
Neste projeto foi codificado em **Java** utilizando set de frameworks **Spring** :
* Boot
* Data
* MVC
* Security

## Banco de dados
MySQL
