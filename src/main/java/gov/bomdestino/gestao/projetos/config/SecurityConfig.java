package gov.bomdestino.gestao.projetos.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@Profile("!test")
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers(HttpMethod.GET, "/projetos*")
                  .permitAll()
                .antMatchers(HttpMethod.POST, "/projetos")
                  .authenticated()
                .antMatchers(HttpMethod.PUT, "/projetos*")
                  .authenticated()
                .antMatchers(HttpMethod.DELETE, "/projetos*")
                  .authenticated()
            .and()
              .oauth2ResourceServer()
                .jwt();
    }
}