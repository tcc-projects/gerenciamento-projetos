package gov.bomdestino.gestao.projetos.controller;

import gov.bomdestino.gestao.projetos.dto.ProjetoDTO;
import gov.bomdestino.gestao.projetos.service.ProjetoService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.net.URI;
import java.util.List;

@RestController
@RequestMapping(value = "/projetos", produces = MediaType.APPLICATION_JSON_VALUE)
@AllArgsConstructor
@CrossOrigin(origins = "*")
public class ProjetoController {
    private final ProjetoService projetoService;

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Criação de novos projetos",
                 authorizations = {@Authorization(value = "acess_token")})
    public ResponseEntity criar(@RequestBody @Valid ProjetoDTO projetoDTO, HttpServletRequest request){
        Long idProjeto = projetoService.salvar(projetoDTO);
        URI uri = ServletUriComponentsBuilder.fromServletMapping(request).path("/projetos/{id}").build()
                                             .expand(idProjeto).toUri();
        return ResponseEntity.created(uri).build();
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Busca um projeto pelo id do mesmo")
    public ProjetoDTO buscaPorId(@PathVariable Long id){
        return projetoService.buscaPorId(id);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Busca todos os projetos cadastrados")
    public List<ProjetoDTO> buscarTodos(){
        return projetoService.buscaTodos();
    }

    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @ApiOperation(value = "Atualiza informações de um projeto existente",
                  authorizations = {@Authorization(value = "acess_token")})
    public void atualizar(@PathVariable Long id, @RequestBody @Valid ProjetoDTO projetoDTO){
        projetoService.editar(projetoDTO, id);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @ApiOperation(value = "Remove um projeto",
                  authorizations = {@Authorization(value = "acess_token")})
    public void remover(@PathVariable Long id){
        projetoService.deletar(id);
    }
}
