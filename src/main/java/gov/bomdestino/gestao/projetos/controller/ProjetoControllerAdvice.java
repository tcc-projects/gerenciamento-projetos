package gov.bomdestino.gestao.projetos.controller;


import gov.bomdestino.gestao.projetos.controller.dto.ErroDTO;
import gov.bomdestino.gestao.projetos.controller.dto.OcorrenciaErroDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDateTime;

@ControllerAdvice
public class ProjetoControllerAdvice {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ErroDTO> methodArgumentReponse(MethodArgumentNotValidException ex) {
        ErroDTO erroDTO = new ErroDTO();
        erroDTO.setDataErro(LocalDateTime.now());
        erroDTO.setHttpCode(400);

        ex.getBindingResult().getAllErrors().forEach(error -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            erroDTO.addOcorrencia(new OcorrenciaErroDTO(errorMessage, fieldName));
        });
        return ResponseEntity.badRequest().body(erroDTO);
    }


    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity methodArgumentReponse(EntityNotFoundException ex) {
        return ResponseEntity.notFound().build();
    }

}
