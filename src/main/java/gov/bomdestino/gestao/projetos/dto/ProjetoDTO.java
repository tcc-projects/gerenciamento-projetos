package gov.bomdestino.gestao.projetos.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Getter
@Setter
@EqualsAndHashCode
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProjetoDTO {
    @ApiModelProperty(hidden = true)
    private Long id;
    @Size(min = 3, max = 100, message = "O Nome do projeto deve ter entre 3 e 100 caracteres")
    @NotNull(message = "O Nome do projeto deve ter entre 3 e 100 caracteres")
    private String nome;
    @Size(min = 10,max = 300, message = "A Descrição do projeto deve ter entre 10 e 500 caracteres")
    @NotNull(message = "A Descrição do projeto deve ter entre 10 e 500 caracteres")
    private String descricao;
    @DecimalMin(value = "1", message = "Orçamento previsto mínimo deve exceder a 0")
    private BigDecimal orcamentoPrevisto;
    private BigDecimal custoAtual;
    @NotNull(message = "Data de inicio é obrigatória")
    private LocalDate dataInicio;
    private LocalDate previsaoEntrega;
    private int andamentoProjeto;
}
