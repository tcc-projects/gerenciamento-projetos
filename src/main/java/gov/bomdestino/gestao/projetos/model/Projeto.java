package gov.bomdestino.gestao.projetos.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.time.LocalDate;

@Getter
@Setter
@Entity
public class Projeto {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String nome;
    private String descricao;
    private BigDecimal orcamentoPrevisto;
    private BigDecimal custoAtual;
    private LocalDate dataInicio;
    private LocalDate previsaoEntrega;
    private int andamentoProjeto;
}
