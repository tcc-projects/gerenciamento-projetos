package gov.bomdestino.gestao.projetos.repository;

import gov.bomdestino.gestao.projetos.model.Projeto;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProjetoRepository extends JpaRepository<Projeto, Long> {
}
