package gov.bomdestino.gestao.projetos.service;

import gov.bomdestino.gestao.projetos.dto.ProjetoDTO;
import gov.bomdestino.gestao.projetos.model.Projeto;
import gov.bomdestino.gestao.projetos.repository.ProjetoRepository;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class ProjetoService {
    private final ProjetoRepository projetoRepository;
    private final ModelMapper modelMapper;

    public Long salvar(ProjetoDTO projetoDTO){
        return projetoRepository.save(modelMapper.map(projetoDTO, Projeto.class)).getId();
    }

    public List<ProjetoDTO> buscaTodos(){
        return  projetoRepository.findAll()
                                 .stream()
                                 .map(projeto -> modelMapper.map(projeto, ProjetoDTO.class))
                                 .collect(Collectors.toList());
    }

    public ProjetoDTO buscaPorId(Long id){
        return  projetoRepository.findById(id)
                .map(projeto -> modelMapper.map(projeto, ProjetoDTO.class))
                .orElseThrow(EntityNotFoundException::new);
    }

    public void editar(ProjetoDTO projetoDTO, Long id){
        Projeto projeto = projetoRepository.findById(id)
                                           .orElseThrow(EntityNotFoundException::new);

        projeto.setAndamentoProjeto(projetoDTO.getAndamentoProjeto());
        projeto.setCustoAtual(projetoDTO.getCustoAtual());
        projeto.setDataInicio(projetoDTO.getDataInicio());
        projeto.setNome(projetoDTO.getNome());
        projeto.setOrcamentoPrevisto(projetoDTO.getOrcamentoPrevisto());
        projeto.setPrevisaoEntrega(projetoDTO.getPrevisaoEntrega());
        projeto.setDescricao(projetoDTO.getDescricao());
        projetoRepository.save(projeto);
    }

    public void deletar(Long id){
        projetoRepository.deleteById(id);
    }
}
