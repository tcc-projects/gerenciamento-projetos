package gov.bomdestino.gestao.projetos.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import gov.bomdestino.gestao.projetos.config.SecurityConfig;
import gov.bomdestino.gestao.projetos.dto.ProjetoDTO;
import gov.bomdestino.gestao.projetos.service.ProjetoService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.oauth2.resource.servlet.OAuth2ResourceServerAutoConfiguration;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import javax.persistence.EntityNotFoundException;

import java.util.ArrayList;
import java.util.List;

import static gov.bomdestino.gestao.projetos.service.ProjetoDTOStubs.projetoDTOSimples;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(value = ProjetoController.class)
@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
class ProjetoControllerTest {

    private static String projetoValidoJson = "{\"nome\":\"Projeto legal de Teste\",\"descricao\":\"Projeto de melhorias\",\"orcamentoPrevisto\":1,\"custoAtual\":0,\"dataInicio\":\"2021-03-01\",\"previsaoEntrega\":\"2021-05-01\",\"andamentoProjeto\":10}";
    private static String projetoInvalido = "{\"nome\":\"  \",\"descricao\":\"Projeto de melhorias\",\"orcamentoPrevisto\":1,\"custoAtual\":0,\"dataInicio\":\"2021-03-01\",\"previsaoEntrega\":\"2021-05-01\",\"andamentoProjeto\":10}";

    @Autowired
    MockMvc http;

    @MockBean
    ProjetoService projetoService;


    @Test
    void deveriaCriarUmNovoProjeto() throws Exception {
        http.perform(post("/projetos")
                     .contentType(MediaType.APPLICATION_JSON)
                     .content(projetoValidoJson))
                .andExpect(status().isCreated())
                .andExpect(header().string("location", "http://localhost/projetos/0"));
        verify(projetoService).salvar(any());
    }

    @Test
    void deveriaRetornarBadRequestComListaDeErrosSeJsonInvalido() throws Exception {
        http.perform(post("/projetos")
                .contentType(MediaType.APPLICATION_JSON)
                .content(projetoInvalido))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.erros.length()").value(greaterThan(0)));

        verifyNoInteractions(projetoService);
    }

    @Test
    void deveriaRetornarUmProjetoPeloId() throws Exception {
        ProjetoDTO projeto = ProjetoDTO.builder().nome("Nome mó legal").build();
        when(projetoService.buscaPorId(1L)).thenReturn(projeto);
        http.perform(get("/projetos/1")
               .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.nome").value("Nome mó legal"));
        verify(projetoService).buscaPorId(eq(1L));
    }

    @Test
    void deveriaRetornarNotFoundSeProjetoNaoEncontrado() throws Exception {
        when(projetoService.buscaPorId(1L)).thenThrow(new EntityNotFoundException());
        http.perform(get("/projetos/1")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
        verify(projetoService).buscaPorId(eq(1L));
    }

    @Test
    void deveriaRetornarOkComUmaListaDeProjetos() throws Exception {
        when(projetoService.buscaTodos()).thenReturn(List.of(new ProjetoDTO(), new ProjetoDTO()));
        http.perform(get("/projetos")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").value(hasSize(2)));
        verify(projetoService).buscaTodos();
    }

    @Test
    void deveriaRetornarOkMesmoSeListaDeProjetosVazia() throws Exception {
        when(projetoService.buscaTodos()).thenReturn(new ArrayList<>());
        http.perform(get("/projetos")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").value(hasSize(0)));
        verify(projetoService).buscaTodos();
    }

    @Test
    void deveriaEditarUmProjeto() throws Exception {
        http.perform(put("/projetos/1")
                .content(projetoValidoJson)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
        verify(projetoService).editar(any(), eq(1L));
    }

    @Test
    void naoDeveriaEditarSeJsonInvalidoERetornarBadRequestComListaDeErros() throws Exception {
        http.perform(put("/projetos/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(projetoInvalido))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.erros.length()").value(greaterThan(0)));
        verifyNoInteractions(projetoService);
    }

    @Test
    void deveriaRemoverUmProjeto() throws Exception {
        http.perform(delete("/projetos/1"))
                .andExpect(status().isNoContent());
        verify(projetoService).deletar(1L);
    }
}