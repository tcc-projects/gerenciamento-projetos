package gov.bomdestino.gestao.projetos.dto;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;


import java.math.BigDecimal;
import java.util.Random;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class ProjetoDTOTest {

    static Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

    @ParameterizedTest(name ="{index} - O nome ''{0}'', não é valido")
    @MethodSource("nomesInvalidos")
    void deveriaApontarErrosDeValidacaoNoNome(String nome){
        var projeto = ProjetoDTO.builder().nome(nome).build();
        ConstraintViolation<ProjetoDTO> nomeViolation = validator.validate(projeto)
                                                                 .stream()
                                                                 .filter(violation -> violation.getPropertyPath()
                                                                                               .toString()
                                                                                               .equals("nome"))
                                                                 .findFirst()
                                                                 .get();

        assertEquals("O Nome do projeto deve ter entre 3 e 100 caracteres", nomeViolation.getMessage());
    }

    @ParameterizedTest(name ="{index} - A descrição ''{0}'', não é valida")
    @MethodSource("descricoesInvalidas")
    void deveriaApontarErrosDeValidacaoNaDescricao(String descricao){
        var projeto = ProjetoDTO.builder().descricao(descricao).build();
        ConstraintViolation<ProjetoDTO> nomeViolation = validator.validate(projeto)
                .stream()
                .filter(violation -> violation.getPropertyPath()
                        .toString()
                        .equals("descricao"))
                .findFirst()
                .get();

        assertEquals("A Descrição do projeto deve ter entre 10 e 500 caracteres", nomeViolation.getMessage());
    }

    @Test
    void deveriaApontarErroDeValidacaoSeOrcamentoPrevistoForBaixo(){
        var projeto = ProjetoDTO.builder().orcamentoPrevisto(BigDecimal.ZERO).build();
        ConstraintViolation<ProjetoDTO> nomeViolation = validator.validate(projeto)
                .stream()
                .filter(violation -> violation.getPropertyPath()
                        .toString()
                        .equals("orcamentoPrevisto"))
                .findFirst()
                .get();

        assertEquals("Orçamento previsto mínimo deve exceder a 0", nomeViolation.getMessage());
    }

    @Test
    void deveriaApontarErroDeValidacaoSedataInicioForNula(){
        var projeto = ProjetoDTO.builder().dataInicio(null).build();
        ConstraintViolation<ProjetoDTO> nomeViolation = validator.validate(projeto)
                .stream()
                .filter(violation -> violation.getPropertyPath()
                        .toString()
                        .equals("dataInicio"))
                .findFirst()
                .get();

        assertEquals("Data de inicio é obrigatória", nomeViolation.getMessage());
    }




    static Stream<String> descricoesInvalidas(){
        return Stream.of(" ", "", null, randomWord(600), randomWord(9));
    }
    static Stream<String> nomesInvalidos(){
        return Stream.of(" ", "", null, randomWord(101), randomWord(2));
    }

    static String randomWord(int length) {
        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = length;
        Random random = new Random();
        StringBuilder buffer = new StringBuilder(targetStringLength);
        for (int i = 0; i < targetStringLength; i++) {
            int randomLimitedInt = leftLimit + (int)
                    (random.nextFloat() * (rightLimit - leftLimit + 1));
            buffer.append((char) randomLimitedInt);
        }
        return buffer.toString();
    }
}