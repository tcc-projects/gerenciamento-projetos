package gov.bomdestino.gestao.projetos.service;

import gov.bomdestino.gestao.projetos.dto.ProjetoDTO;
import gov.bomdestino.gestao.projetos.model.Projeto;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

public class ProjetoDTOStubs {

    public static List<ProjetoDTO> projetoDTOList(){
        return List.of(projetoDTOSimples());
    }

    public static ProjetoDTO projetoDTOSimples(){
        ProjetoDTO dto = new ProjetoDTO();
        dto.setId(1l);
        dto.setNome("Projeto legal de Teste");
        dto.setPrevisaoEntrega(LocalDate.now());
        dto.setAndamentoProjeto(10);
        dto.setOrcamentoPrevisto(BigDecimal.ONE);
        dto.setDataInicio(LocalDate.now());
        dto.setCustoAtual(BigDecimal.ZERO);
        dto.setDescricao("Projeto de melhorias");
        return dto;
    }

    static ProjetoDTO projetoDTOComplexo(){
        ProjetoDTO dto = new ProjetoDTO();
        dto.setId(2l);
        dto.setNome("Projeto muito COMPLEXO");
        dto.setPrevisaoEntrega(LocalDate.of(2021,3,26));
        dto.setAndamentoProjeto(99);
        dto.setOrcamentoPrevisto(BigDecimal.TEN);
        dto.setDataInicio(LocalDate.now());
        dto.setCustoAtual(BigDecimal.TEN);
        dto.setDescricao("Projeto de complexidades");
        return dto;
    }

    static List<Projeto> projetoEntityList(){
        return List.of(projetoEntitySimples());
    }

    static Projeto projetoEntitySimples(){
        Projeto entity = new Projeto();
        entity.setId(1l);
        entity.setNome("Projeto legal de Teste");
        entity.setPrevisaoEntrega(LocalDate.now());
        entity.setAndamentoProjeto(10);
        entity.setOrcamentoPrevisto(BigDecimal.ONE);
        entity.setDataInicio(LocalDate.now());
        entity.setCustoAtual(BigDecimal.ZERO);
        entity.setDescricao("Projeto de melhorias");
        return entity;
    }
}
