package gov.bomdestino.gestao.projetos.service;

import gov.bomdestino.gestao.projetos.dto.ProjetoDTO;
import gov.bomdestino.gestao.projetos.model.Projeto;
import gov.bomdestino.gestao.projetos.repository.ProjetoRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

import static gov.bomdestino.gestao.projetos.service.ProjetoDTOStubs.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ProjetoServiceTest {

    @Mock
    ProjetoRepository projetoRepository;

    @Captor
    ArgumentCaptor<Projeto> projetoCaptor;

    static ModelMapper modelMapper = new ModelMapper();

    ProjetoService projetoService;

    @BeforeEach
    void init(){
        projetoService = new ProjetoService(projetoRepository, modelMapper);
    }

    @Test
    void deveriaSalvarUmProjeto() {
        var projeto = new ProjetoDTO();
        projeto.setNome("Projeto de Teste");
        var entidade = new Projeto();
        entidade.setId(1L);

        when(projetoRepository.save(any())).thenReturn(entidade);

        Long idEntidade = projetoService.salvar(projeto);

        verify(projetoRepository).save(projetoCaptor.capture());
        assertEquals("Projeto de Teste", projetoCaptor.getValue().getNome());
        assertEquals(entidade.getId(), idEntidade);
    }

    @Test
    void deveriaBuscarTodosOsProjetosETransformarEmDTOS() {
        when(projetoRepository.findAll()).thenReturn(projetoEntityList());
        List<ProjetoDTO> projetosEncontrados = projetoService.buscaTodos();
        assertEquals(projetoDTOList(), projetosEncontrados);
        verify(projetoRepository).findAll();
    }

    @Test
    void deveriaLancarExcecaoSeProjetoNaoForEncontrado() {
        when(projetoRepository.findById(1L)).thenReturn(Optional.empty());
        assertThrows(EntityNotFoundException.class, () -> projetoService.buscaPorId(1L));
    }

    @Test
    void deveriaBuscarUmProjetoPeloId() {
        when(projetoRepository.findById(1L)).thenReturn(Optional.of(projetoEntitySimples()));
        ProjetoDTO projetoEncontrado = projetoService.buscaPorId(1L);
        assertEquals(projetoDTOSimples(), projetoEncontrado);
    }

    @Test
    void deveriaEncontrarUmProjetoEditar() {
        when(projetoRepository.findById(1L)).thenReturn(Optional.of(projetoEntitySimples()));
        ProjetoDTO novosDados = projetoDTOComplexo();
        projetoService.editar(novosDados, 1L);

        verify(projetoRepository).findById(1L);
        verify(projetoRepository).save(projetoCaptor.capture());
        Projeto projetoAlterado = projetoCaptor.getValue();

        assertEquals(1L, projetoAlterado.getId());
        assertEquals(novosDados.getNome(), projetoAlterado.getNome());
        assertEquals(novosDados.getPrevisaoEntrega(), projetoAlterado.getPrevisaoEntrega());
        assertEquals(novosDados.getAndamentoProjeto(), projetoAlterado.getAndamentoProjeto());
        assertEquals(novosDados.getOrcamentoPrevisto(), projetoAlterado.getOrcamentoPrevisto());
        assertEquals(novosDados.getDataInicio(), projetoAlterado.getDataInicio());
        assertEquals(novosDados.getCustoAtual(), projetoAlterado.getCustoAtual());
        assertEquals(novosDados.getDescricao(), projetoAlterado.getDescricao());
    }

    @Test
    void deveriaLancarExceptionAoTentarEditarSeProjetoNaoForEncontrado() {
        when(projetoRepository.findById(1L)).thenReturn(Optional.empty());
        ProjetoDTO projetoParaAlterar = projetoDTOComplexo();
        assertThrows(EntityNotFoundException.class, ()->  projetoService.editar(projetoParaAlterar, 1L));
    }

    @Test
    void deveriaDeletarUmProjeto() {
        projetoService.deletar(1L);
        verify(projetoRepository).deleteById(1L);
    }
}